# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Valter Mura <valtermura@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-08 18:54+0200\n"
"Last-Translator: Valter Mura <valtermura@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../contributors_manual.rst:5
msgid "Contributors Manual"
msgstr "Manuale dei collaboratori"

#: ../../contributors_manual.rst:7
msgid "Everything you need to know to help out with Krita!"
msgstr "Tutto quello che devi sapere per collaborare con Krita!"

#: ../../contributors_manual.rst:9
msgid "Contents:"
msgstr "Contenuto:"
