# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-07-03 13:49+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/dockers/animation_docker.rst:1
msgid "Overview of the animation docker."
msgstr "Overzicht van de vastzetter Animatie."

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation"
msgstr "Animatie"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation Playback"
msgstr "Animatie afspelen"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Play/Pause"
msgstr "Afspelen/pauzeren"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Framerate"
msgstr "Framesnelheid"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "FPS"
msgstr "FPS"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Speed"
msgstr "Snelheid"

#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Animation Docker"
msgstr "Vastzetter Animatie"

#: ../../reference_manual/dockers/animation_docker.rst:18
msgid ".. image:: images/dockers/Animation_docker.png"
msgstr ".. image:: images/dockers/Animation_docker.png"

#: ../../reference_manual/dockers/animation_docker.rst:19
msgid ""
"To have a playback of the animation, you need to use the animation docker."
msgstr "Om een animatie af te spelen heeft u de vastzetter Animatie nodig."

#: ../../reference_manual/dockers/animation_docker.rst:21
msgid ""
"The first big box represents the current Frame. The frames are counted with "
"programmer's counting so they start at 0."
msgstr ""
"Het eerste grote vak representeert het huidige frame. De frames worden "
"geteld met telling door programmeurs, ze beginnen dus bij 0."

#: ../../reference_manual/dockers/animation_docker.rst:23
msgid ""
"Then there are two boxes for you to change the playback range here. So, if "
"you want to do a 10 frame animation, set the end to 10, and then Krita will "
"cycle through the frames 0 to 10."
msgstr ""
"Er zijn dan twee vakken voor u om de afspeelreeks hier te wijzigen. Dus, als "
"u een animatie wilt doen net 10 frames, stel het einde in op 10 en Krita zal "
"cirkelen door de frames 0 tot 10."

#: ../../reference_manual/dockers/animation_docker.rst:25
msgid ""
"The bar in the middle is filled with playback options, and each of these can "
"also be hot-keyed. The difference between a keyframe and a normal frame in "
"this case is that a normal frame is empty, while a keyframe is filled."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:27
msgid ""
"Then, there's buttons for adding, copying and removing frames. More "
"interesting is the next row:"
msgstr ""
"Er zijn dan knoppen voor toevoegen, kopiëren en verwijderen van frames. "
"Interessanter is de volgende rij:"

#: ../../reference_manual/dockers/animation_docker.rst:29
msgid "Onion Skin"
msgstr "Uienschil"

#: ../../reference_manual/dockers/animation_docker.rst:30
msgid "Opens the :ref:`onion_skin_docker` if it wasn't open before."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:31
msgid "Auto Frame Mode"
msgstr "Automatische framemodus"

#: ../../reference_manual/dockers/animation_docker.rst:32
msgid ""
"Will make a frame out of any empty frame you are working on. Currently "
"automatically copies the previous frame."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid "Drop frames"
msgstr "Frames laten vallen"

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid ""
"This'll drop frames if your computer isn't fast enough to show all frames at "
"once. This process is automatic, but the icon will become red if it's forced "
"to do this."
msgstr ""

#: ../../reference_manual/dockers/animation_docker.rst:36
msgid ""
"You can also set the speedup of the playback, which is different from the "
"framerate."
msgstr ""
