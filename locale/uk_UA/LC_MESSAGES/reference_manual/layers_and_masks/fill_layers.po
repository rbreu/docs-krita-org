# Translation of docs_krita_org_reference_manual___layers_and_masks___fill_layers.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___fill_layers\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:50+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:0
msgid ".. image:: images/layers/fill_layer_simplex_noise.png"
msgstr ".. image:: images/layers/fill_layer_simplex_noise.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:1
msgid "How to use fill layers in Krita."
msgstr "Як користуватися шарами заповнення у Krita."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Layers"
msgstr "Шари"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Fill"
msgstr "Заповнення"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:12
msgid "Generator"
msgstr "Породжувач"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:17
msgid "Fill Layers"
msgstr "Шари заповнення"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:19
msgid ""
"A Fill Layer is a special layer that Krita generates on-the-fly that can "
"contain either a pattern or a solid color."
msgstr ""
"Шар заповнення — спеціалізований шар, який Krita створює на льоту і який "
"може містити або заповнення візерунком, або заповнення суцільним кольором."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:22
msgid ".. image:: images/layers/Fill_Layer.png"
msgstr ".. image:: images/layers/Fill_Layer.png"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:24
msgid ""
"This fills the layer with a predefined pattern or texture that has been "
"loaded into Krita through the Resource Management interface.  Patterns can "
"be a simple and interesting way to add texture to your drawing or painting, "
"helping to recreate the look of watercolor paper, linen, canvas, hardboard, "
"stone or an infinite other number of options.  For example if you want to "
"take a digital painting and finish it off with the appearance of it being on "
"canvas you can add a Fill Layer with the Canvas texture from the texture "
"pack below and set the opacity very low so the \"threads\" of the pattern "
"are just barley visible.  The effect is quite convincing."
msgstr ""
"Використання цього варіанта заповнює шар попередньо визначеним візерунком "
"або текстурою, яку завантажено до Krita за допомогою інтерфейсу керування "
"ресурсами. Візерунки можуть бути простим і цікавими способом додавання "
"текстури до вашого малюнка або картини, який допоможе відтворити ефект "
"акварельного паперу, льняного полотна, звичайного полотна, деревної плити, "
"каменю або будь-якого іншого матеріалу. Наприклад, якщо ви хочете створити "
"цифровий малюнок і надати йому вигляд намальованого на полотні, ви можете "
"додати шар заповнення із текстурою полотна з пакунка текстури під шаром "
"малювання і встановити дуже низьке значення непрозорості так, щоб «волокна» "
"візерунка були ледве видимими. Ефект буде доволі переконливим."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid "Pattern"
msgstr "Візерунок"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:26
msgid ""
"You can create your own and use those as well.  For a great set of well "
"designed and useful patterns check out one of our favorite artists and a "
"great friend of Krita, David Revoy's free texture pack (https://www."
"davidrevoy.com/article156/texture-pack-1)."
msgstr ""
"Ви також можете створити власний візерунок і скористатися ним. Чудовий набір "
"якісних і корисних візерунків можна знайти в одного з наших улюблених "
"художників і великих друзів Krita, Давіда Реоя (David Revoy). Ось його "
"безкоштовний пакунок текстур: https://www.davidrevoy.com/article156/texture-"
"pack-1"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:29
msgid ""
"The second option is not quite as exciting, but does the job. Fill the layer "
"with a selected color."
msgstr ""
"Другий варіант не такий творчий, але виконує своє завдання. Заповнення шару "
"вибраним кольором."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:36
msgid ""
"A noise generator that isn't Perline Noise (which is what typical 'clouds' "
"generation is), but it looks similar and can actually loop. Uses the "
"OpenSimplex code."
msgstr ""
"Генератор шуму, який не є шумом Перліна (шумом, який є типовим у «хмарах»), "
"але виглядає подібно і може породжувати циклічні структури. Використовує код "
"OpenSimplex."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:38
msgid "Looping"
msgstr "Цикл"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:39
msgid "Whether or not to force the pattern to loop."
msgstr "Визначає, чи слід примусово циклічно повторювати візерунок."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:40
msgid "Frequency"
msgstr "Частота"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:41
msgid ""
"The frequency of the waves used to generate the pattern. Higher frequency "
"results in a finer noise pattern."
msgstr ""
"Частота хвиль для створення візерунка. Вища частота дає тендітнішу структуру "
"шуму."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:42
msgid "Ratio"
msgstr "Пропорція"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:43
msgid ""
"The ratio of the waves in the x and y dimensions. This makes the noise have "
"a rectangular appearance."
msgstr ""
"Співвідношення хвиль у напрямках x та y. Надає шуму прямокутної структури."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Simplex Noise"
msgstr "Симплекс-шум"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid "Use Custom Seed"
msgstr "Використати нетиповий ініціатор"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:45
msgid ""
"The seed for the random component. You can input any value or text here, and "
"it will always try to use this value to generate the random values with "
"(which then are always the same for a given seed)."
msgstr ""
"Початкове число для компоненти випадкових чисел. У це поле ви можете ввести "
"будь-яке значення або текст. Програма скористається цим значенням для "
"створення послідовності псевдовипадкових чисел (яка завжди є однаковою для "
"того самого початкового числа)."

#: ../../reference_manual/layers_and_masks/fill_layers.rst:48
msgid "Painting on a fill layer"
msgstr "Малювання на шарі заповнення"

#: ../../reference_manual/layers_and_masks/fill_layers.rst:50
msgid ""
"A fill-layer is a single-channel layer, meaning it only has transparency. "
"Therefore, you can erase and paint on fill-layers to make them semi-opaque, "
"or for when you want to have a particular color only. Being single channel, "
"fill-layers are also a little bit less memory-consuming than regular 4-"
"channel paint layers."
msgstr ""
"Шар заповнення є одноканальним шаром. Це означає, що він може мати лише "
"прозорі ділянки. Тому ви можете витирати зображення або малювання на шарах "
"заповнення, щоб зробити їх напівпрозорими або заповненими лише одним "
"кольором. Оскільки ці шари є одноканальними, на зберігання їхніх даних "
"витрачається дещо менше пам'яті, ніж на звичайні 4-канальні шари малювання."
