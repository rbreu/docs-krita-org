msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___blending_modes___misc.pot\n"

#: ../../reference_manual/blending_modes/misc.rst:1
msgid ""
"Page about the miscellaneous blending modes in Krita: Bumpmap, Combine "
"Normal Map, Copy Red, Copy Green, Copy Blue, Copy and Dissolve."
msgstr ""
"介绍 Krita 的其他类混色模式，包括：凹凸贴图、组合法线贴图、复制红通道、复制绿"
"通道、复制蓝通道、复制、溶解等。"

#: ../../reference_manual/blending_modes/misc.rst:15
msgid "Misc"
msgstr "其他"

#: ../../reference_manual/blending_modes/misc.rst:17
msgid "Bumpmap (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:21
msgid "Bumpmap"
msgstr "凹凸贴图"

#: ../../reference_manual/blending_modes/misc.rst:23
msgid "This filter seems to both multiply and respect the alpha of the input."
msgstr ""
"对上下两层颜色数据进行 :ref:`bm_multiply` ，但考虑输入数据的透明度通道。"

#: ../../reference_manual/blending_modes/misc.rst:25
#: ../../reference_manual/blending_modes/misc.rst:30
msgid "Combine Normal Map"
msgstr "组合法线贴图"

#: ../../reference_manual/blending_modes/misc.rst:25
msgid "Normal Map"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:32
msgid ""
"Mathematically robust blending mode for normal maps, using `Reoriented "
"Normal Map Blending <https://blog.selfshadow.com/publications/blending-in-"
"detail/>`_."
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:34
msgid "Copy (Blending Mode)"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:38
msgid "Copy"
msgstr "复制"

#: ../../reference_manual/blending_modes/misc.rst:40
msgid ""
"Copies the previous layer exactly. Useful for when using filters and filter-"
"masks."
msgstr "将完全复制上一个图层的内容，在使用滤镜图层和滤镜蒙版时有用。"

#: ../../reference_manual/blending_modes/misc.rst:46
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:46
msgid "Left: **Normal**. Right: **Copy**."
msgstr "左： **正常** ； 右： **复制** 。"

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Red"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Green"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:48
msgid "Copy Blue"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:54
msgid "Copy Red, Green, Blue"
msgstr "复制红通道、绿通道、蓝通道"

#: ../../reference_manual/blending_modes/misc.rst:56
msgid ""
"This is a blending mode that will just copy/blend a source channel to a "
"destination channel. Specifically, it will take the specific channel from "
"the upper layer and copy that over to the lower layers."
msgstr "把上面图层的一个颜色通道的数据复制到下面图层中。"

#: ../../reference_manual/blending_modes/misc.rst:59
msgid ""
"So, if you want the brush to only affect the red channel, set the blending "
"mode to 'copy red'."
msgstr ""
"因此如果你想让笔刷效果只影响到红通道，可以把上层的混色模式设为“复制红通道”。"

#: ../../reference_manual/blending_modes/misc.rst:65
msgid ""
".. image:: images/blending_modes/misc/Krita_Filter_layer_invert_greenchannel."
"png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:65
msgid "The copy red, green and blue blending modes also work on filter-layers."
msgstr "复制红通道、绿通道、蓝通道也可以在滤镜图层上使用。"

#: ../../reference_manual/blending_modes/misc.rst:67
msgid ""
"This can also be done with filter layers. So if you quickly want to flip a "
"layer's green channel, make an invert filter layer with 'copy green' above "
"it."
msgstr ""
"你也可以在滤镜图层上使用这些混色模式。例如，如果你想单独对一个图层的绿通道进"
"行反相，你可以在这个图层上面新建一个“反相”滤镜图层，然后把它的混色模式设为“复"
"制绿通道”。"

#: ../../reference_manual/blending_modes/misc.rst:72
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Red_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:72
msgid "Left: **Normal**. Right: **Copy Red**."
msgstr "左： **正常** ； 右： **复制红通道** 。"

#: ../../reference_manual/blending_modes/misc.rst:78
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Green_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:78
msgid "Left: **Normal**. Right: **Copy Green**."
msgstr "左： **正常** ； 右： **复制绿通道** 。"

#: ../../reference_manual/blending_modes/misc.rst:84
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Copy_Blue_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:84
msgid "Left: **Normal**. Right: **Copy Blue**."
msgstr "左： **正常** ； 右： **复制蓝通道** 。"

#: ../../reference_manual/blending_modes/misc.rst:86
#: ../../reference_manual/blending_modes/misc.rst:90
msgid "Dissolve"
msgstr "溶解"

#: ../../reference_manual/blending_modes/misc.rst:92
msgid ""
"Instead of using transparency, this blending mode will use a random "
"dithering pattern to make the transparent areas look sort of transparent."
msgstr "使用随机抖动的图案来使得真透明区域变成伪透明。"

#: ../../reference_manual/blending_modes/misc.rst:97
msgid ""
".. image:: images/blending_modes/misc/"
"Blending_modes_Dissolve_Sample_image_with_dots.png"
msgstr ""

#: ../../reference_manual/blending_modes/misc.rst:97
msgid "Left: **Normal**. Right: **Dissolve**."
msgstr "左： **正常** ； 右： **溶解** 。"
